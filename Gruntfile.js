module.exports = function(grunt) {

  require('load-grunt-tasks')(grunt);

  grunt.initConfig({
   autoprefixer: {
    options: {
      browsers: ['last 2 versions', 'ie 8', 'ie 9']
    },
    target: {
      src: 'app/css/main.css'
    }
   },
   postcss: {
    options: {
        processors: [
          require('cssgrace'),
        ]
      },
    target: {
      src: 'app/css/main.css'
    }
   },
   sass: {
    target: {
      files: {
        'app/css/main.css':'src/sass/main.scss'
      }
    }
   },
   cssmin: {
    target: {
      files: {
        'app/css/main.min.css':'app/css/main.css'
      }
    }
   },
   watch: {
    sass: {
      files: ['src/**/*.scss'],
      tasks: ['css'],
      options: {
        livereload: true
      }
    },
    html: {
      files: ['**/*.html'],
      options: {
        livereload: true
      }
    }
   }
  });

  grunt.registerTask('default', []);
  grunt.registerTask('css', ['sass', 'autoprefixer', 'postcss', 'cssmin']);

}